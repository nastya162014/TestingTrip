﻿using System;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace RZD
{
    
    /// <summary>
    /// Тестовое задание от ГК Экстрим
    /// </summary>
    [CodedUITest]
    public class FindBestWay
    {
        private TestContext testContextInstance;
        private IWebDriver driver;
       
     
        [TestMethod]
        [TestCategory("Chrome")]
        public void TheSearchTest()
        {
           
             //Найти дату для ввода в поле "дата поездки" с помощью свойства  AccountDays (найти ближайшую субботу)
              TimeTrip reqData = new TimeTrip();          
               reqData.AccountDays = DateTime.Now;          
               DateTime accountDays = reqData.AccountDays;
               // обработать время из условий задания необходимое для выбора наилучшего рейса (не ранне 12) 
               DateTime parsedReqTime = DateTime.ParseExact(ValuesForFinding.reqTime, "HH:mm", null);
               driver.Manage().Window.Maximize();
               //открыть необходимую страницу 
               driver.Navigate().GoToUrl(UrlsPage.baseUrl);


               IWebElement aStill = driver.FindElement(By.XPath("//a[. =  'ещё']"));        
               aStill.Click();

               IWebElement aRasp = driver.FindElement(By.CssSelector("a[href='https://rasp.yandex.ru/']"));
               aRasp.Click();
               Assert.IsTrue(driver.Url.Contains(UrlsPage.RaspUrl));
               //выбрать тип транспорта электричка
               TransportType.SelectTransportType(driver);
               //заполнить поля Откуда Куда Когда
               RouteForm.FillRouteForm(driver);


               if (IsElementExists.CatchNoElntExct(driver,By.CssSelector("h1[class='SearchNotFound__title']")))
               {
                   Console.WriteLine(driver.FindElement(By.CssSelector("h1[class='SearchNotFound__title']")).Text);//do if exists
               }
               else
               {
                 HeadingRoute.AssertHeadingRoute(driver, Values.headingRasp, By.CssSelector("header[class='SearchTitle']"));
                 ResultsRaspTable resultsRaspTable = new ResultsRaspTable();
                 resultsRaspTable.GetResultsRaspTable(driver);


            }


            //Найти заголовок таблицы с информацией о  рейсах 
            /*   IWebElement SearchHeaderTitle = driver.FindElement(By.CssSelector("header[class='SearchTitle']"));


               //Проверить, что в заголовке таблицы есть названия пункта назначения и отправления 


               //Сохранить время из таблицы результатов
           /*    var SearchSpansTime = driver.FindElements(By.CssSelector("div[class='SearchSegment__dateTime Time_important']"));
               //Сохранить цену из таблицы результатов
               var SearchSpansPrice = driver.FindElements(By.CssSelector("div[class='SearchSegment__scheduleAndPrices SearchSegment__scheduleAndPrices_fullWidth']"));
               //Сохранить информацию о транспорте из таблицы результатов
               var SearchSpansTransp = driver.FindElements(By.CssSelector("span[class='SegmentTitle__title']"));


               //Получить и обработать цену в тип int
               List<int> priceTrips = new List<int>();
               foreach (IWebElement x in SearchSpansPrice)
               {                
                   string patternRUB = @"\b(\d+\W?Р)";
                   string patternPrice = @"\d+";
                   Regex regex = new Regex(patternRUB);               
                   Match match = regex.Match(x.Text); 
                   string priceString = match.ToString();
                   regex = new Regex(patternPrice);
                   match = regex.Match(priceString);
                   priceString = match.ToString();
                   priceTrips.Add(Int32.Parse(priceString));
               }

               //Перевести в тип DateTime данные о времени из таблицы результатов
               List<DateTime> timeTrips = new List<DateTime>();
               timeTrips.Add(DateTime.ParseExact(SearchSpansTime[0].Text.Remove(0, SearchSpansTime[0].Text.Length - 5), "HH:mm", null));
               for (int i = 1; i < SearchSpansTime.Count-1; i++)                
               {
                   
               }


               //Отсортировать для удобного поиска необходимого времени
               var SorttimeTrips = timeTrips.OrderBy(d => d).ToList();


               int reqRecord = -1;
               int j = 0;
               int idrecord;
               //Поиск наилучшего рейса, 1) не ранне 12-00, не больше 200 руб (строгие неравенства)
               while ( (reqRecord < 0) || (j < SorttimeTrips.Count))
               {

                    if (SorttimeTrips[j].CompareTo(parsedReqTime) == 1) 
                    {
                        idrecord = timeTrips.IndexOf(SorttimeTrips[j]);
                        if (priceTrips[idrecord] < ValuesForFinding.reqPriceTrip)
                        {
                            reqRecord = j;
                            Console.WriteLine("Рекомендуемое время и цена для выбора:");
                            Console.WriteLine(SorttimeTrips[j].ToString("HH:mm"));
                            Console.WriteLine(  priceTrips[idrecord] + " руб");
                            SearchSpansTransp[idrecord].Click();
                            Console.WriteLine(driver.Title + "-Заголовок страницы");
                            break;
                        }
                    }                               
                    j++;               
               }
               //В случае, если нет налучшего рейса вывести сообщение 
               if (reqRecord < 0)
               {
                   Console.WriteLine("Извините, но поездка с условиями не ранее 12-00 и билет не более 200 не найдена!");

               }*/
        }

        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestInitialize()]
        public void SetupTest()
        {
            driver = currDriver.LaunchDriver();
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            //driver.Quit();
        }
    }
}
