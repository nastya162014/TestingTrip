﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RZD
{
    public static class Values
    {
        public static string toCity = "Каменск-Уральский";
        public static string fromCity = "Екатеринбург";
        public static string AssertTitle = "из Екатеринбурга в Каменск-Уральский";
        public static string transportType = "Электричка";
        public static int dollar = 60;
        public static string headingRasp = "из Екатеринбурга-Пасс. в Каменск-Уральский";
        public static string headingBestWay = "Екатеринбург-Пасс. — Каменск-Уральский";
    }
    public static class ValuesForFinding
    {
        public static string FieldToCity = "toName";
        public static string FieldFromCity = "fromName";
        public static string FieldDate = "date-input_search__input";
        public static string reqTime = "12:00";
        public static int reqPriceTrip = 200;
      

    }
    public class TimeTrip
    {
        private DateTime accountDays;
        private int reqDayOfWeek = 6; //Нам нужна суббота, 0 -воскресенье; 1 - 6 : понедельник - суббота
        public DateTime AccountDays
        {
            get
            {               
                 int dayOfWeek =(int)accountDays.DayOfWeek;
                 if ((dayOfWeek > 0) && (dayOfWeek < reqDayOfWeek))
                 {
                    accountDays = accountDays.AddDays(reqDayOfWeek - dayOfWeek);

                 }
                 if (dayOfWeek == reqDayOfWeek)
                 {
                    accountDays = accountDays.AddDays(7);
                 }
                 if (dayOfWeek > reqDayOfWeek)
                 {
                    accountDays = accountDays.AddDays(reqDayOfWeek + (7 - dayOfWeek));
                 }
                if ((dayOfWeek == 0) &&  (reqDayOfWeek != 0))
                {
                    accountDays = accountDays.AddDays(reqDayOfWeek);
                }
                return accountDays;
            }
            set
            {
                accountDays = value;
            }
        }
    }
}
